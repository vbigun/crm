<?php

namespace App\Http\Controllers;

use App\Trader;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function trader(Request $request)
    {
        $data = $request->all();
        $trader = Trader::create($data);
        return $trader;
    }
}
