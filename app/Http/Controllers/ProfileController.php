<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;


class ProfileController extends Controller
{
    public function getProfile ()
    {
        return JWTAuth::toUser(JWTAuth::getToken());
        //return 'qwerty';
    }
}
