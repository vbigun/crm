<?php

namespace App\Http\Controllers;

use App\Trader;
use App\User;
use Illuminate\Http\Request;

class TraderController extends Controller
{
    public function list()
    {
        $list = Trader::all();
        foreach($list as $item)
        {
            $item["manager_name"] = User::find($item["manager_id"])["name"];
        }
        return $list;
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $trader = Trader::create($data);
        return $trader;
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        $trader = Trader::findOrFail($id);
        if(!empty($data["manager_name"]))
            unset($data["manager_name"]);
        $trader->update($data);
        return response()->json('Successfully updated');
    }

    public function destroy($id)
    {
        $trader = Trader::find(intval($id));
        $trader->delete();
        return response()->json('Successfully deleted');
    }
}
