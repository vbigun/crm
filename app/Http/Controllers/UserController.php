<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class UserController extends Controller
{
    public function list()
    {
        return [
            [
                'id' => 1,
                'name' => 'Super admin',
                'email' => 'superadmin@gmail.com',
                'role_id' => 1,
                'date' => '21.05.2019'
            ],
            [
                'id' => 2,
                'name' => 'Admin',
                'email' => 'admin@gmail.com',
                'role_id' => 2,
                'date' => '03.06.2019'
            ],
            [
                'id' => 3,
                'name' => 'Trader Denis',
                'email' => 'denis@gmail.com',
                'role_id' => 3,
                'date' => '18.06.2019'
            ],
            [
                'id' => 4,
                'name' => 'Trader Masha',
                'email' => 'masha@gmail.com',
                'role_id' => 3,
                'date' => '16.05.2019'
            ],
            [
                'id' => 5,
                'name' => 'Trader Boris',
                'email' => 'boris@gmail.com',
                'role_id' => 3,
                'date' => '30.04.2019'
            ],
        ];
    }

    public function index () {
        //return User::where('role_id', '=', '2')->get();
        return User::all();
    }

    public function getManagersList()
    {
        return User::where('role_id', '=', '3')->get();
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);
        return $user;
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        //$data['password'] = Hash::make($data['password']);
        $user = User::findOrFail($id);
        $user->update($data);
        return response()->json('Successfully updated');
    }

    public function destroy($id)
    {
        $user = User::find(intval($id));
        $user->delete();
        return response()->json('Successfully deleted');
    }
}
