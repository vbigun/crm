<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trader extends Model
{
    protected $fillable = [
        'name', 'email', 'manager_id', 'balance', 'factor'
    ];
}
