<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 150; $i++)
        {
            DB::table('orders')->insert([
                'client_id' => rand(1, 25),
                'product_id' => rand(1, 3),
                'status_id' => rand(1, 4),
                'date' => date('Y-m-d', mt_rand(1546329600, time())),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);

            DB::table('cards')->insert([
                'client_id' => rand(1, 25),
                'product_id' => rand(1, 3),
                'status_id' => rand(1, 3),
                'credit_limit' => rand(1000, 25000),
                'date' => date('Y-m-d', mt_rand(1546329600, time())),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ]);
        }
    }
}
