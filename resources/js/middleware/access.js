import axios from "axios";
import * as types from "../store/mutation-types";

export default (to, from, next) => {
    // if (store.getters['auth/user'].role !== 'admin') {
    //console.log('!admin');
    async function fetchUser() {
        try {
            const {data} = await axios.post('/api/www/profile');
            return data.role_id;
        } catch (e) {
            console.log('NET');
            return false;
        }
    }

    fetchUser().then(function(role){
        if (role)
        {
            if (role == 1) {
                console.log('Admin');
                next ();
            }
            else {
                console.log('!admin');
            }
        }
    });
}
