import Vue from 'vue'
import VueBreadcrumbs from "vue-breadcrumbs";

Vue.use(VueBreadcrumbs, {template: '<ol class="breadcrumb" v-if="$breadcrumbs.length"><router-link class="breadcrumb-item" tag="li" v-for="(crumb, key) in $breadcrumbs" :to="linkProp(crumb)" :key="key">' +
        '<a v-if="(typeof crumb.meta.breadcrumb) == \'object\'">{{ $t(crumb.meta.breadcrumb.title, crumb.meta.breadcrumb.params) }}</a>' +
        '<a v-if="(typeof crumb.meta.breadcrumb) == \'string\'">{{ $t(crumb.meta.breadcrumb) }}</a>' +
        '</router-link></ol>'});
