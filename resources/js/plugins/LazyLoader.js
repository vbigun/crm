import Vue from 'vue'

const LazyLoader = {
    install: function (Vue) {
        Vue.lazyLoad = Vue.prototype.$lazyLoad = function (src) {
            return new Promise(function (resolve, reject) {
                let el = {};

                if (src.endsWith('js')) {
                    el = document.createElement('script');
                    el.type = 'text/javascript';
                    el.async = true;
                    el.src = src;

                    if (document.querySelector('script[src="' + src + '"]')) {
                        resolve();
                        return;
                    }
                } else if (src.endsWith('css')) {
                    el = document.createElement('link');
                    el.rel = 'stylesheet';
                    el.href = src;

                    if (document.querySelector('link[href="' + src + '"]')) {
                        resolve();
                        return;
                    }
                } else {
                    reject();
                    return;
                }

                // console.log(el);

                el.addEventListener('load', resolve);
                el.addEventListener('error', reject);
                el.addEventListener('abort', reject);

                document.head.appendChild(el);
            });
        };

        Vue.lazyUnload = Vue.prototype.$lazyUnload = function (src) { // eslint-disable-line no-param-reassign
            return new Promise(function (resolve, reject) {
                var el = null;
                if (src.endsWith('js')) {
                    el = document.querySelector('script[src="' + src + '"]');
                } else if (src.endsWith('css')) {
                    el = document.querySelector('link[href="' + src + '"]');
                }

                if (!el) {
                    reject();

                    return;
                }

                document.head.removeChild(el);

                resolve();
            });
        };
    },
};

Vue.use(LazyLoader);
