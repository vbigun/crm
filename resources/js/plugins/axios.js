import axios from 'axios'
import store from '../store'
import router from '../router'
import Swal from 'sweetalert2'
import i18n from '../plugins/i18n'

// Request interceptor
axios.interceptors.request.use(request => {
    console.log(request.method + ' axios: ' + request.url);
    const token = store.getters['auth/token'];
    if (token) {
        // console.log('use token '+ token);
        request.headers.common['Authorization'] = `Bearer ${token}`
    }

    const locale = store.getters['lang/locale'];
    if (locale) {
        request.headers.common['Accept-Language'] = locale
    }

    return request
});

// Response interceptor
axios.interceptors.response.use(response => {
    // console.log(response.headers);
    if (response.headers['authorization']) {
        let token = response.headers['authorization'].split(' ');
        // console.log("new token store: " + token[1])
        store.dispatch('auth/saveToken', {
            token: token[1],
            remember: false
        });
    }

    // todo check new token header

    return response;
}, error => {
    const {status} = error.response;

    // console.log("error interceptors: " + JSON.stringify(error.response.data));

    if (status >= 500) {
        Swal.fire({
            type: 'error',
            title: i18n.t('error_alert_title'),
            text: i18n.t('error_alert_text'),
            reverseButtons: true,
            confirmButtonText: i18n.t('ok'),
            cancelButtonText: i18n.t('cancel')
        })
    }

    if (status === 401 && store.getters['auth/check']) {
        Swal.fire({
            type: 'warning',
            title: i18n.t('token_expired_alert_title'),
            text: i18n.t('token_expired_alert_text'),
            reverseButtons: true,
            confirmButtonText: i18n.t('ok'),
            cancelButtonText: i18n.t('cancel')
        }).then(() => {
            store.commit('auth/LOGOUT');

            router.push({name: 'welcome'})
        })
    }

    if (status === 422) {
        let jsonData = error.response.data;

        // console.log(jsonData);

        var message = '';
        for (var errors in jsonData.errors) {
            for (var error in jsonData.errors[errors]) {
                if (typeof (jsonData.errors[errors][error]) == "object")
                    message += jsonData.errors[errors][error].message + "<br>";
                else message += jsonData.errors[errors][error] + "<br>";
            }
        }

        // for (let i = 0; i < jsonData.errors.length; i++) {
        //     console.log(typeof (jsonData.errors[i]));
        // }

        Swal.fire({
            type: 'warning',
            title: jsonData.message,
            html: message,
            reverseButtons: true,
            confirmButtonText: i18n.t('ok'),
            cancelButtonText: i18n.t('cancel')
        });
    }


    return Promise.reject(error)
});
