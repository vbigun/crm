import Vue from 'vue'

const Helpers = {
    install(Vue, opts) {
        Vue.prototype.$toggleBodyClass = function (className, status) {
            if (status === true) {
                if (!document.body.classList.contains(className))
                    document.body.classList.add(className);
            } else {
                if (document.body.classList.contains(className))
                    document.body.classList.remove(className);
            }
        }
    }
}

Vue.use(Helpers);
