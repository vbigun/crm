function page(path) {
    return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}

export default [
    {path: '/', name: 'welcome', component: page('welcome.vue')},
    {path: '/register', name: 'register', component: page('register.vue'), meta: {breadcrumb: "Register"}},
    {
        path: '/dashboard',
        name: 'dashboard',
        component: page('dashboard.vue'),
        meta: {breadcrumb: 'dashboard'}
    },
    {
        path: '/desks',
        name: 'desks',
        component: page('desks.vue'),
        meta: {breadcrumb: 'desks'}
    },
    {
        path: '/trasnactions',
        name: 'trasnactions',
        component: page('trasnactions.vue'),
        meta: {breadcrumb: 'trasnactions'}
    },
    {
        path: '/trades', 
        name: 'trades',
        component: page('trades.vue'),
        meta: {breadcrumb: 'trades'}
    },
    {
        path: '/tickets',
        name: 'tickets',
        component: page('tickets.vue'),
        meta: {breadcrumb: 'tickets'}
    },
    {
        path: '/reports',
        name: 'reports',
        component: page('reports.vue'),
        meta: {breadcrumb: 'reports'}
    },
    {
        path: '/analytics',
        name: 'analytics',
        component: page('analytics.vue'),
        meta: {breadcrumb: 'analytics'},
    },
    {
        path: '/campaigns',
        name: 'campaigns',
        component: page('campaigns.vue'),
        meta: {breadcrumb: 'campaigns'}
    },
    {
        path: '/mailer',
        name: 'mailer',
        component: page('mailer.vue'),
        meta: {breadcrumb: 'mailer'}
    },
    {
        path: '/layouts',
        name: 'layouts',
        component: page('layouts.vue'),
        meta: {breadcrumb: 'layouts'}
    },
    {
        path: '/workers',
        name: 'workers',
        component: page('workers.vue'),
        meta: {breadcrumb: 'workers'}
    },
    {
        path: '/chats',
        name: 'chats',
        component: page('chats.vue'),
        meta: {breadcrumb: 'chats'}
    },
    {
        path: '/calendar',
        name: 'calendar',
        component: page('calendar.vue'),
        meta: {breadcrumb: 'calendar'}
    },
    {
        path: '/communications',
        name: 'communications',
        component: page('communications.vue'),
        meta: {breadcrumb: 'communications'}
    },
    {
        path: '/logs',
        name: 'logs',
        component: page('logs.vue'),
        meta: {breadcrumb: 'logs'}
    },
    {
        path: '/positions',
        name: 'positions',
        component: page('positions.vue'),
        meta: {breadcrumb: 'positions'}
    },
    {
        path: '/system/index',
        component: page('system/template.vue'),
        meta: {breadcrumb: 'system'},
        children: [
            {
                name: 'system.authorization',
                path: '/system/authorization',
                component: page('system/authorization.vue'),
                meta: {breadcrumb: 'authorization'},
            },
            {
                name: 'system.index',
                path: '',
                component: page('system/index.vue'),
            }
        ]
    },
    {path: '*', component: page('errors/404.vue')}
]
