import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    managers: null
};

// getters
export const getters = {
    managers: state => state.managers,
    check: state => state.managers !== null
};

// mutations
export const mutations = {
    [types.MANAGERS_GET](state, {managers}) {
        state.managers = managers
    },
};

// actions
export const actions = {
    async getManagers({commit}) {
        try {
            const {data} = await axios.post('/api/www/users/managers');
            commit(types.MANAGERS_GET, {managers: data})
        } catch (e) {
        }
    },
};
