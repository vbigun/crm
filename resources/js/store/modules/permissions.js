import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    permissions: null
};

// getters
export const getters = {
    permissions: state => state.permissions,
    check: state => state.permissions !== null
};

// mutations
export const mutations = {
    [types.PERMISSIONS_GET](state, {permissions}) {
        state.permissions = permissions
    },
};

// actions
export const actions = {
    async getPermissions({commit}) {
        try {
            const {data} = await axios.post('/api/www/authorization/permissions/index');
            commit(types.PERMISSIONS_GET, {permissions: data})
        } catch (e) {
            console.log(e);
        }
    },
};
