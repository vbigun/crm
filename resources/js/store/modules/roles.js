import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    roles: null
};

// getters
export const getters = {
    roles: state => state.roles,
    check: state => state.roles !== null
};

// mutations
export const mutations = {
    [types.ROLES_GET](state, {roles}) {
        state.roles = roles
    }
};

// actions
export const actions = {
    async getRoles({commit}) {
        try {
            const {data} = await axios.post('/api/www/authorization/roles/index');
            commit(types.ROLES_GET, {roles: data})
        } catch (e) {
            console.log(e);
        }
    },
};
