import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    traders: null
};

// getters
export const getters = {
    traders: state => state.traders,
    check: state => state.traders !== null
};

// mutations
export const mutations = {
    [types.TRADERS_GET](state, {traders}) {
        state.traders = traders
    },
};

// actions
export const actions = {
    async getTraders({commit}) {
        // let id = 3;
        try {
            const {data} = await axios.post('/api/www/traders/list');
            commit(types.TRADERS_GET, {traders: data})
        } catch (e) {
        }
    },
};
