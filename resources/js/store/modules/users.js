import axios from 'axios'
import * as types from '../mutation-types'

// state
export const state = {
    users: null
};

// getters
export const getters = {
    users: state => state.users,
    check: state => state.users !== null
};

// mutations
export const mutations = {
    [types.USERS_GET](state, {users}) {
        state.users = users
    },
};

// actions
export const actions = {
    async getUsers({commit}) {
        try {
            const {data} = await axios.post('/api/www/users/list');
            commit(types.USERS_GET, {users: data})
        } catch (e) {
        }
    },
};
