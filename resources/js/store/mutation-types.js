
// auth.js
export const LOGOUT = 'LOGOUT';
export const SAVE_TOKEN = 'SAVE_TOKEN';
export const FETCH_USER = 'FETCH_USER';
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS';
export const FETCH_USER_FAILURE = 'FETCH_USER_FAILURE';
export const UPDATE_USER = 'UPDATE_USER';

// lang.js
export const SET_LOCALE = 'SET_LOCALE';

//users.js
export const USERS_GET = 'USERS_GET';

//managers.js
export const MANAGERS_GET = 'MANAGERS_GET';

//traders.js
export const TRADERS_GET = 'TRADERS_GET';

// authorization
export const ROLES_GET = 'ROLES_GET';
export const PERMISSIONS_GET = 'PERMISSIONS_GET';
