<?php

//-----> API
Route::post('api/www/inside/trader', 'ApiController@trader');

//-----> Users
Route::post('api/www/users/list', 'UserController@index');
Route::post('api/www/users/managers', 'UserController@getManagersList');
Route::post('api/www/users/create', 'UserController@store');
Route::post('api/www/users/edit/{id}', 'UserController@update');
Route::post('api/www/users/delete/{id}', 'UserController@destroy');

//-----> Traders
Route::post('api/www/traders/list', 'TraderController@list');
Route::post('api/www/traders/create', 'TraderController@store');
Route::post('api/www/traders/edit/{id}', 'TraderController@update');
Route::post('api/www/traders/delete/{id}', 'TraderController@destroy');

//-----> Roles
Route::post('api/www/authorization/roles/index', 'RolesController@index');
Route::post('api/www/authorization/roles/create', 'RolesController@store');
Route::post('api/www/authorization/roles/edit/{id}', 'RolesController@update');
Route::post('api/www/authorization/roles/delete/{id}', 'RolesController@destroy');

//-----> Permissions
Route::post('api/www/authorization/permissions/index', 'PermissionsController@index');
Route::post('api/www/authorization/permissions/create', 'PermissionsController@store');
Route::post('api/www/authorization/permissions/edit/{id}', 'PermissionsController@update');
Route::post('api/www/authorization/permissions/delete/{id}', 'PermissionsController@destroy');

//-----> Auth
Route::post('api/www/auth/login', 'AuthController@login');
Route::post('api/www/profile', 'ProfileController@getProfile');

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('auth/user', 'AuthController@user');
    
});
Route::post('api/www/auth/register', 'AuthController@register');



Route::group([
    'prefix' => 'restricted',
    'middleware' => 'auth:api',
], function () {
    Route::get('logout', 'Auth\LoginController@logout');
    Route::get('/test', function () {
        return 'authenticated';
    });
});

Route::group(['middleware' => 'jwt.refresh'], function(){
    Route::get('auth/refresh', 'AuthController@refresh');
});

Route::get('{path}', function () {
    return view('index');
})->where('path', '(.*)');

Route::post('{path}', function () {
    return view('index');
})->where('path', '(.*)');
